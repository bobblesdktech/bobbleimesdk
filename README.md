# BobbleIMESDK

This guide is for all app developers who wish to add a custom keyboard functionality in their Android apps using the Bobble IME (Input Method Editor) SDK

Note - Minimum version of the supported Android platform is SDK level 21

# Table of Contents
1. [Adding BobbleIME SDK to your project](#step1)
2. [Adding permissions](#step2)
3. [Manifest changes](#step3)
4. [Other build settings](#step4)
5. [Initialise SDK](#step5)
6. [Create your custom IME Class](#step6)
7. [Customize keyboard top bar](#step7)
8. [Other APIs](#step8)

## <a name="step1"></a>Step 1: Adding the BobbleIME SDK to your Project
Pull the latest version of the SDK from Maven as described below:

- Include Maven in your settings.gradle file along with the credentials - read URL, username, and password (Shared on mail):

```groovy
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        maven {
            url "http://dl.appnext.com/"
        }
        maven {
            url mavenReadUrl
            credentials {
                username mavenReadUsername
                password mavenReadPassword
            }
        }
    }
}

```
- Add the following line to the dependencies element in your application module’s build.gradle. (Update the version name as per shared on mail)

```groovy
implementation 'com.touchtalent.bobblekeyboard:keyboard:99.9.9.999'
```

- Sync your Gradle project to ensure that the dependency is downloaded by the build system.


## <a name="step2"></a>Step 2: Adding permissions
### Granting permissions

The SDK uses the permissions granted to your app in order to improve the typing experience. In order to suggest the most relevant content to your users,
we highly recommend that your app request the following permissions so that we can personalize user experience in a better way:
```xml
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
<uses-permission android:name="android.permission.RECORD_AUDIO" />
<uses-permission android:name="android.permission.READ_CONTACTS" />
```
    
    
## <a name="step3"></a>Step 3: Manifest changes
The client needs to register the custom IME class in manifest as InputMethod service.

```xml
<service
    android:name=".services.CustomIME"
    android:exported="true"
    android:label="@string/custom_input_method"
    android:permission="android.permission.BIND_INPUT_METHOD">
    
    <intent-filter>
        <action android:name="android.view.InputMethod" />
    </intent-filter>

    <meta-data
        android:name="android.view.im"
        android:resource="@xml/bobble_ime" />
</service>
```

## <a name="step4"></a>Step 4: Other Build Settings
- Add option to not compress dictionary files by following lines in the android block of your gradle
```groovy
aaptOptions {
    noCompress ".dict"
}
```

## <a name="step5"></a>Step 5: Initialise SDK
Inside onCreate() method of your Application class, initialize the SDK by calling
```kotlin
BobbleIMESDK.initialise(this) 
```


## <a name="step6"></a>Step 6: Create your custom IME Class
Create a custom class declared in the manifest above.
```kotlin
import com.touchtalent.bobbleime.services.BobbleIME;

class CustomIME : BobbleIME() {
    // override methods here
}
```
## <a name="step7"></a>Step 7: Configure keyboard top bar
### Add top bar icons
```kotlin
// Enum class containing list of all possible icons
enum TopIcon {
    SETTINGS,
    VOICE,
    THEME,
    CLIPBOARD,
    FONT,
    STICKERS,
    JOKES,
    SHAYARIS,
    SHORTCUTS,
    QUOTES,
    MY_ICON;
}

// Config model to customize look and feel of the top bar
data class TopBarConfig(
    val leftIcons: List<TopIcon>, /*list of left side icons will be added*/
    val rightIcons: List<TopIcon>, /*list of right side icons will be added*/
    val leftMaxIcons: Int = 6, /*max icon present in left side, default is 6*/
    val rightMaxIcons: Int = 2, /*max icon present in right side, default is 2*/
    val myIconSize: Int = 1 /*size of MY_ICON, Default size is 1. Size N means N*32 dp width*/
)

// Function to customize top bar
object BobbleIMESDK {
    fun setTopBarIcons(topBarIcons: TopBarConfig)
}
```
This method is used to customize keyboard top bar icons.
<br>
There are 2 types of icons:
1. Bobble Icons (Generic keyboard icons - Customisation, Stickers, Fonts, Voice, etc) see `TopIcon` for complete list
2. Configurable Icon (MY_ICON)
  - Only a single custom configurable icon can be provided by you.
  - Size of the configurable icon is determined in multiples of standard size of 32dp - 1x, 2x, 3x, etc. 
<br>
To ensure a smooth UX experience, following constraints apply on the customisation of top bar. An exception is thrown, if any of the constraint is not fulfilled:
1. More than a total of 7 icons are added (MY_ICON size is counted as per its size. E.g - 3X size counts a 3 icons)
2. Passed list has any duplicate icons 

Example:

```kotlin
val leftIcons = listOf(BobbleIMESDK.TopIcon.MY_ICON)
val rightIcons = listOf(BobbleIMESDK.TopIcon.FONT, BobbleIMESDK.TopIcon.STICKERS)
BobbleIMESDK.setTopBarIcons(TopBarConfig(
                leftIcons = leftIcons,
                rightIcons = rightIcons,
                leftMaxIcons = 3,
                rightMaxIcons = 4,
                myIconSize = 2
            ))
```

> P.S This method must be called after `BobbleIMESDK.initialise()`

### 2. Provide view for MY_ICON (If added)

There are 2 states in a keyboard:
1. Default state
    - Active by default when keyboard opens on a text field
    - Active when no text is input, or cursor is not near any text
    - Size of MY_ICON is unrestricted in this state
2. Typing state
    - Visible when some text is input, and suggestions are visible on the top bar.
    - Size of MY_ICON is restricted to 1 in this state    
<br>

```kotlin
class CustomIME : BobbleIME() {
    override fun onCreateTopView(isDark: Boolean): View {
        return TopBarView(this).apply {
            setColorMode(isDark)
            topBarView = this
            setOnClickListener {
                showMyCustomView()
            }
        }
    }

    override fun onCreateSmallTopView(isDark: Boolean): View {
        return TopBarSmallView(this).apply {
            setColorMode(isDark)
            topBarSmallView = this
            setOnClickListener {
                showMyCustomView()
            }
        }
    }

    override fun canShowMyIcon(): Boolean {
        return true
    }
}
```

```kotlin
override fun onCreateTopView(isDark: Boolean): View?
```
Override this method in `CustomIME` to create view for __MY_ICON__, which will be shown in the keyboard default state. If the method returns `null` then MY_ICON  will not be added to the keyboard. Parameter: `isDark` specify if the keyboard theme is dark. UI of the view can be controlled using this parameter.

```kotlin    
override fun onCreateSmallTopView(isDark: Boolean): View?
```
Override this method in `CustomIME` to create a small view for __MY_ICON__, which will be shown on the right side in typing state. If this method returns `null` then MY_ICON will not be shown in typing state. Parameter: `isDark` specify if the keyboard theme is dark. UI of the view can be controlled using this parameter.

```kotlin
override fun canShowMyIcon(): Boolean
```
Override this method to change the visibility of __MY_ICON__. The default value is `false` so `CustomIME` class needs to override this to return true for showing MY_ICON.

## <a name="step8"></a>Step 8: Other APIs
### setSpaceBarIcon(@DrawableRes spaceBarIcon: Int)
This API allows to replace Branding icon on keyboard of Bobble to a custom icon of your choice. Recommended asset configurations:
1. Dimensions of the asset should be equal to or less than 21x21. 
2. Ensure that the asset has minimal padding on all corners, as it will be taken care by the IME itself

Example:
```koltin
BobbleIMESDK.setSpaceBarIcon(R.drawable.logo)
```

### setSpaceBarText(@StringRes spaceBarText: Int)

This API allows to add your text to the keyboard space bar. Recommended guidelines for the strings:
1. Keep the text length small (≤7 characters), else the text might overflow on spacebar UI
> P.S: In case, the language text is large, text might be hidden, and only icon would be shown in that case

Example:
```kotlin
 BobbleIMESDK.setSpaceBarText(R.string.keyboard_name)
```

### onStartIME()

The custom class that extends BobbleIME, may override `onStartIME()`. This is called when the IME is triggered by the system and is visible to the user on  a edit field.

Example:
```kotlin
class CustomIME : BobbleIME() {
    internal val scope = MainScope()
    private var timerJob: Job? = null

    // Sample implementation to demonstrate updating timer whenever keyboard is open
    override fun onStartIME() {
        super.onStartIME()
        timerJob?.cancel()
        timerJob = scope.launch {
            while (true) {
                topBarView?.updateTimer(dateFormat.format(Date()))
                delay(1000)
            }
        }
    }
```
### onFinishIME()

The custom class that extends BobbleIME, may override `onFinishIME()`. This is called when the IME service is triggered by the system and it's about to hide.

Example:
```kotlin
    // Cancel the timer job, whenever keyboard closes
    override fun onFinishIME() {
        super.onFinishIME()
        timerJob?.cancel()
    }
```

### showCustomView(view: View, isTransparent: Boolean, height: Int)

The custom class that extends BobbleIME can call `showCustomView()` API to show a custom view in the custom view container. When this method is called, the keyboard input view is hidden and the custom view is shown. If already a custom view is visible, it is replaced by the newly provided custom view.

Parameters:
- `view` - instance of customView that needs to be added.
- `isTransparent` - `true` if the view background needs to be transparent (keyboard visible underneath). Default: `false`
- `height` - height in pixels required by the view. Default: `-1` expands the height of the keyboard intelligently for the best UX. 

Example:
```kotlin
val customView = MyCustomView(this)
showCustomView(customView, false, -1)
```
### showKeyboardView()

The custom class that extends BobbleIME can call `showKeyboardView()` API to show the keyboard input view in case it was hidden due to the previous call to `showCustomView()` and the custom view was visible. Calling `showKeyboardView()` will close the visible custom view (if any) and show the keyboard input view.

### commitTextToInput(text: String, send: Boolean)

This API allows you to send a text to the currently active input connection of the keyboard.

Parameters:
- `text` - String needs to be committed
- `send` - `true` if the text directly needs to be sent to the app where IME is open, `false` if the text will only be added to editText of the app.
> P.S: `send` parameter depends on the parent app and works on best chance possible, it might not be respected.

Example:
```kotlin
customIme.commitTextToInput("Hello World", false)
```
### commitContentToInput(contentUri: Uri, mime: String): Boolean

This method will be used to share to the open app (or optionally a specified destination app) which has triggered the keyboard. This method will send across the content passed to it to the destination app.

Parameter:
- `contentUri` - Content Uri of the content
- `mime` - MIME type of the content

E.g: 
```kotlin
    binding.shareImage.setOnClickListener {
        keyboardService.scope.launch {
            val success = withContext(Dispatchers.IO) {
                val ios = context.assets.open("sample_sticker.webp")
                val file = File(context.filesDir, "sample_sticker.webp")
                val fos = FileOutputStream(file)
                val byteArray = ByteArray(1024)
                while (ios.available() != 0) {
                    val read = ios.read(byteArray)
                    fos.write(byteArray, 0, read)
                }
                val uri = FileProvider.getUriForFile(
                    context,
                    "com.touchtalent.bobble.keyboard.sample.fileprovider",
                    file
                )
                keyboardService.commitContentToInput(uri, "image/webp.wasticker")
            }
            if (!success)
                Toast.makeText(
                    context,
                    "Parent app doesn't support this format",
                    Toast.LENGTH_SHORT
                ).show()
        }
    }
```

package com.touchtalent.bobble.keyboard.sample

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.touchtalent.bobbleime.sdk.BobbleIMESDK
import com.touchtalent.bobbleime.sdk.TopBarConfig

class KeyboardPlayGroundApp : Application() {
    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        setupBobbleSdk()
    }

    private fun setupBobbleSdk() {
        BobbleIMESDK.initialise(this)

        val leftIcons = listOf(BobbleIMESDK.TopIcon.MY_ICON)
        val rightIcons = listOf(BobbleIMESDK.TopIcon.STICKERS, BobbleIMESDK.TopIcon.FONT)
        BobbleIMESDK.setTopBarIcons(TopBarConfig(leftIcons, rightIcons, 3, 4, 3))

        BobbleIMESDK.setSpaceBarIcon(R.drawable.logo)
        BobbleIMESDK.setSpaceBarText(R.string.keyboard_name)
    }
}
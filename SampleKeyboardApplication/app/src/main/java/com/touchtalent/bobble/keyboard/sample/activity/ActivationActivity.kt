package com.touchtalent.bobble.keyboard.sample.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.touchtalent.bobble.keyboard.sample.R

/**
 * This activity is the entry point for starting the enabling flow.
 * Users can be shown some graphics along with feature list and a button to enable the keyboard
 */
class ActivationActivity : BobbleEnablerActivity(), View.OnClickListener {

    private lateinit var install: Button
    private lateinit var tryOut: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_bobble)
        install = findViewById(R.id.install)
        tryOut = findViewById(R.id.tryOut)
        install.setOnClickListener(this)
        //Set the initial status of the UI according to SDK status
        setStatus(getStatus())
    }

    private fun setStatus(status: IMEInstallStatus) {
        install.isFocusable = true
        install.isClickable = true
        when (status) {
            IMEInstallStatus.NONE -> {
                install.text = "Enable My Keyboard(Step-1)"
                install.visibility = View.VISIBLE
                tryOut.visibility = View.GONE
            }
            IMEInstallStatus.ENABLED -> {
                install.text = "Enable My Keyboard(Step-2)"
                install.visibility = View.VISIBLE
                tryOut.visibility = View.GONE
            }
            IMEInstallStatus.SELECTED -> {
                install.visibility = View.GONE
                tryOut.visibility = View.VISIBLE
            }
        }
    }

    /**
     * During the flow, a system activity is opened, which persists in the activity backstack
     * and cannot be closed due to system limitations. Hence consider bringing up your parent activity to top
     * and close this activity manually when user presses back button
     */
    override fun onBackPressed() {
        finish()
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        startActivity(intent)
    }

    /**
     * Start the activation flow when user presses the button
     */
    override fun onClick(v: View) {
        startActivationFlow()
    }

    /**
     * Callback to keep track of updates made in enabling flow
     * @param imeInstallStatus new modified status
     */
    override fun onStatusChange(imeInstallStatus: IMEInstallStatus) {
        setStatus(imeInstallStatus)
        Log.d("ActivationActivity", "onStatusChange: " + imeInstallStatus.name)
    }

    /**
     * Callback to notify that user has passed the onboarding screen.
     * Consider closing this activity and sending the user back to your parent activity.
     */
    override fun onUserSetup() {
        Log.d("ActivationActivity", "onUserSetup()")
        onBackPressed()
    }

    companion object {
        private val TAG = ActivationActivity::class.java.simpleName
    }
}
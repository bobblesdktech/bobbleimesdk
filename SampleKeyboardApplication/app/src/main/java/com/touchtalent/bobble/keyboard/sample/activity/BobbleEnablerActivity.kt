package com.touchtalent.bobble.keyboard.sample.activity

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.touchtalent.bobble.keyboard.sample.R
import com.touchtalent.bobble.keyboard.sample.activity.BobbleEnablerActivity.IMEInstallStatus.*

open class BobbleEnablerActivity : AppCompatActivity() {

    private lateinit var checkEnabled: Runnable
    private lateinit var checkEnabledHandler: Handler
    private lateinit var imeInstallStatus: IMEInstallStatus
    private var isForeground = false
    private var isSettingsPageLaunched = false
    private var showImeChooserCalled = false
    private var firstLaunch = true
    private var requestCode = 1234
    private var shouldShowImeSwitcher = false
    private var isImeSwitcherShown = false

    enum class IMEInstallStatus {
        ENABLED, NONE, SELECTED
    }

    open fun getStatus(): IMEInstallStatus {
        val imeManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        val listOfKeyboards = imeManager.enabledInputMethodList
        for (i in listOfKeyboards) {
            if (i.packageName.compareTo(packageName) == 0) {
                return if (i.id == Settings.Secure.getString(
                        contentResolver,
                        Settings.Secure.DEFAULT_INPUT_METHOD
                    )
                ) {
                    SELECTED
                } else ENABLED
            }
        }
        return NONE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkEnabled = Runnable {
            val status = getStatus();
            if (this.imeInstallStatus != status) {
                if (status == ENABLED) {
                    checkEnabledHandler.removeCallbacks(checkEnabled);
                    if (!isForeground) {
                        finish()
                        val intent = Intent(this, this::class.java)
                        intent.putExtra("showImeSwitcher", true)
                        startActivity(intent)
                    }
                }
            } else checkEnabledHandler.postDelayed(checkEnabled, 300)
        }
        checkEnabledHandler = Handler(Looper.getMainLooper())
        shouldShowImeSwitcher = intent.getBooleanExtra("showImeSwitcher", false)
        if (shouldShowImeSwitcher) {
            onStatusChange(
                NONE,
                ENABLED
            )
        }
        imeInstallStatus = getStatus()
    }

    override fun onResume() {
        super.onResume()
        if (!firstLaunch) {
            checkEnabledHandler.removeCallbacks(checkEnabled)
        } else firstLaunch = false
        if (isSettingsPageLaunched) {
            isSettingsPageLaunched = false
            val status = getStatus()
            if (this.imeInstallStatus == NONE && status == ENABLED)
                showIMEChooser()
            onStatusChange(this.imeInstallStatus, status)
            this.imeInstallStatus = status
        }
        isForeground = true
    }

    override fun onPause() {
        super.onPause()
        isForeground = false
    }

    protected fun startActivationFlow() {
        checkEnabledHandler.removeCallbacks(checkEnabled)
        when (imeInstallStatus) {
            NONE -> {
                launchEnableKeyboardPage();
            }
            ENABLED -> {
                showIMEChooser()
            }
            SELECTED -> {
                onStatusChange(
                    SELECTED,
                    SELECTED
                )
            }
        }
    }

    private fun launchEnableKeyboardPage() {
        isSettingsPageLaunched = true
        val enableIntent = Intent(Settings.ACTION_INPUT_METHOD_SETTINGS)
        enableIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(enableIntent)
        Handler(Looper.getMainLooper()).postDelayed({
            val intent = Intent(applicationContext, KeyBoardBottomEducationActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            if (intent.resolveActivity(packageManager) != null) {
                startActivity(intent)
            }
        }, 500)
        checkEnabledHandler.postDelayed(checkEnabled, 300)
    }

    private fun showIMEChooser() {
        showImeChooserCalled = true
        val imeManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imeManager.showInputMethodPicker()
    }

    private fun onStatusChange(
        old: IMEInstallStatus,
        aNew: IMEInstallStatus
    ) {
        if (old == NONE && aNew == NONE)
            showKeyboardEnableBackDialog(object : DialogButtonClickListener {
                override fun onPositiveButtonClick() {
                    launchEnableKeyboardPage()
                }

                override fun onNegativeButtonClick() {
                    onStatusChange(NONE)
                }

            })
        if (old == NONE && aNew == ENABLED) {
            // Keyboard enabled
        }
        if (old == ENABLED && aNew == ENABLED) {
            onStatusChange(ENABLED)
        }
        if (aNew == SELECTED) {
            onStatusChange(SELECTED)
        }
    }

    protected open fun onStatusChange(imeInstallStatus: IMEInstallStatus) {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == this.requestCode)
            onUserSetup()
    }

    protected open fun onUserSetup() {

    }

    protected fun isKeyboardOnboardingInProgress(): Boolean {
        return shouldShowImeSwitcher && (!isImeSwitcherShown || showImeChooserCalled)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (shouldShowImeSwitcher && !isImeSwitcherShown && hasFocus) {
            checkEnabledHandler.post(this::showIMEChooser)
            isImeSwitcherShown = true
        }
        if (showImeChooserCalled && hasFocus) {
            val status = getStatus()
            onStatusChange(this.imeInstallStatus, status)
            this.imeInstallStatus = status
            showImeChooserCalled = false
        }
    }

    open fun showKeyboardEnableBackDialog(listener: DialogButtonClickListener) {
        val settingsSelectionDialog = Dialog(this, R.style.PrivacyDialog)
        try {
            settingsSelectionDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val window = settingsSelectionDialog.window ?: return
        window.setGravity(Gravity.CENTER)
        window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        settingsSelectionDialog.setCanceledOnTouchOutside(true)
        settingsSelectionDialog.setContentView(R.layout.dialog_enable_keyboard_back)
        window.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        val buttonOK = settingsSelectionDialog.findViewById<Button>(R.id.okDialog)
        val buttonCancel = settingsSelectionDialog.findViewById<Button>(R.id.cancelDialog)
        buttonOK.setOnClickListener { v: View? ->
            if (!buttonOK.isEnabled) {
                return@setOnClickListener
            }
            listener.onPositiveButtonClick()
            if (settingsSelectionDialog.isShowing) {
                settingsSelectionDialog.dismiss()
            }
        }
        buttonCancel.setOnClickListener { v: View? ->
            listener.onNegativeButtonClick()
            if (settingsSelectionDialog.isShowing) {
                settingsSelectionDialog.dismiss()
            }
        }
        settingsSelectionDialog.setCancelable(true)
        try {
            settingsSelectionDialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    interface DialogButtonClickListener {
        fun onPositiveButtonClick()
        fun onNegativeButtonClick()
    }

}
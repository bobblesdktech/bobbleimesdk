package com.touchtalent.bobble.keyboard.sample.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.touchtalent.bobble.keyboard.sample.R

/**
 * This is a activity which will be deep linked into the keyboard via a branded icon.
 */
class DeepLinkActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deep_link)
    }
}
package com.touchtalent.bobble.keyboard.sample.activity

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.view.View.MeasureSpec
import android.widget.TextView
import com.touchtalent.bobbleapp.R
import com.touchtalent.bobbleapp.activities.BobbleBaseActivity

class KeyBoardBottomEducationActivity : BobbleBaseActivity(), View.OnClickListener {

    private var mHasResume = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
            WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
        )
        window.setFlags(
            WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
            WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
        )
        val view =
            LayoutInflater.from(this).inflate(R.layout.enable_bobble_toast_popup, null, false)
        val screenWidth = resources.displayMetrics.widthPixels
        val screenHeight = resources.displayMetrics.heightPixels
        val window = window
        val wlp = window.attributes
        window.decorView.setPadding(0, 0, 0, 0)
        wlp.gravity = Gravity.BOTTOM
        wlp.width = screenWidth
        view.measure(
            MeasureSpec.makeMeasureSpec(screenWidth, MeasureSpec.EXACTLY),
            MeasureSpec.makeMeasureSpec(screenHeight, MeasureSpec.AT_MOST)
        )
        wlp.height = view.measuredHeight
        window.attributes = wlp
        setContentView(R.layout.enable_bobble_toast_popup)
        Handler(Looper.getMainLooper()).postDelayed({ finish() }, 3000)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
    }

    override fun onResume() {
        super.onResume()
        if (mHasResume) {
            Handler().postDelayed({ finish() }, 1000)
        }
        mHasResume = true
    }

    override fun onStop() {
        super.onStop()
        finish()
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        finish()
        return false
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
//        OnboardingEventUtils.logEnableKBAnimationTap();
        return false
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onClick(v: View) {
        finish()
    }
}
package com.touchtalent.bobble.keyboard.sample.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.touchtalent.bobble.keyboard.sample.R

/**
 * This activity represents a Activity of your app where promotional banners for the keyboard are being shown.
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<View>(R.id.launch).setOnClickListener { v: View? ->
            val intent = Intent(this, ActivationActivity::class.java)
            startActivity(intent)
        }
        findViewById<View>(R.id.webView).setOnClickListener { v: View? ->
            startActivity(
                Intent(
                    this,
                    WebViewActivity::class.java
                )
            )
        }
    }
}
package com.touchtalent.bobble.keyboard.sample.activity

import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.touchtalent.bobble.keyboard.sample.R

class WebViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.webview)
        (findViewById<View>(R.id.webView) as WebView).loadUrl("https://www.google.com")
        val client: WebViewClient = object : WebViewClient() {
            @Deprecated("Deprecated in Java")
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return false
            }
        }
        (findViewById<View>(R.id.webView) as WebView).webViewClient = client
    }
}
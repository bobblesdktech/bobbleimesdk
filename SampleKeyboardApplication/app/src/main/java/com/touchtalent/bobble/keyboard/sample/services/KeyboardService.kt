package com.touchtalent.bobble.keyboard.sample.services

import android.content.Intent
import android.view.View
import com.touchtalent.bobble.keyboard.sample.activity.DeepLinkActivity
import com.touchtalent.bobble.keyboard.sample.services.view.MyCustomView
import com.touchtalent.bobble.keyboard.sample.services.view.TopBarSmallView
import com.touchtalent.bobble.keyboard.sample.services.view.TopBarView
import com.touchtalent.bobbleime.services.BobbleIME
import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * A Sample app to show how to use BobbleIME SDK.
 *
 */
class KeyboardService : BobbleIME() {

    internal val scope = MainScope()
    private var timerJob: Job? = null

    private var topBarView: TopBarView? = null
    private var topBarSmallView: TopBarSmallView? = null

    private val dateFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())

    override fun onCreateTopView(isDark: Boolean): View {
        return TopBarView(this).apply {
            setColorMode(isDark)
            topBarView = this
            setOnClickListener {
                showMyCustomView()
            }
        }
    }

    override fun onCreateSmallTopView(isDark: Boolean): View {
        return TopBarSmallView(this).apply {
            setColorMode(isDark)
            topBarSmallView = this
            setOnClickListener {
                showMyCustomView()
            }
        }
    }

    private fun showMyCustomView() {
        val customView = MyCustomView(this)
        showCustomView(customView, false, -1)
    }

    override fun canShowMyIcon(): Boolean {
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        scope.cancel()
    }

    override fun onStartIME() {
        super.onStartIME()
        timerJob?.cancel()
        timerJob = scope.launch {
            while (true) {
                topBarView?.updateTimer(dateFormat.format(Date()))
                delay(1000)
            }
        }
    }

    override fun onFinishIME() {
        super.onFinishIME()
        timerJob?.cancel()
    }
}
package com.touchtalent.bobble.keyboard.sample.services.view

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.FileProvider
import com.touchtalent.bobble.keyboard.sample.databinding.LayoutMyCustomViewBinding
import com.touchtalent.bobble.keyboard.sample.databinding.LayoutTopSmallViewBinding
import com.touchtalent.bobble.keyboard.sample.databinding.LayoutTopViewBinding
import com.touchtalent.bobble.keyboard.sample.services.KeyboardService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream

class MyCustomView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    private val keyboardService: KeyboardService
        get() = context as KeyboardService

    private val binding = LayoutMyCustomViewBinding.inflate(LayoutInflater.from(context), this)

    init {
        setBackgroundColor(Color.WHITE)
        binding.addText.setOnClickListener {
            keyboardService.commitTextToInput("Hello World", false)
        }
        binding.shareText.setOnClickListener {
            keyboardService.commitTextToInput("Hello World", true)
        }
        binding.shareImage.setOnClickListener {
            keyboardService.scope.launch {
                val success = withContext(Dispatchers.IO) {
                    val ios = context.assets.open("sample_sticker.webp")
                    val file = File(context.filesDir, "sample_sticker.webp")
                    val fos = FileOutputStream(file)
                    val byteArray = ByteArray(1024)
                    while (ios.available() != 0) {
                        val read = ios.read(byteArray)
                        fos.write(byteArray, 0, read)
                    }
                    val uri = FileProvider.getUriForFile(
                        context,
                        "com.touchtalent.bobble.keyboard.sample.fileprovider",
                        file
                    )
                    keyboardService.commitContentToInput(uri, "image/webp.wasticker")
                }
                if (!success)
                    Toast.makeText(
                        context,
                        "Parent app doesn't support this format",
                        Toast.LENGTH_SHORT
                    ).show()
            }
        }
    }

}
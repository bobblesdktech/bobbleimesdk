package com.touchtalent.bobble.keyboard.sample.services.view

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.touchtalent.bobble.keyboard.sample.databinding.LayoutTopSmallViewBinding
import com.touchtalent.bobble.keyboard.sample.databinding.LayoutTopViewBinding

class TopBarSmallView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    private val binding = LayoutTopSmallViewBinding.inflate(LayoutInflater.from(context), this)

    fun setColorMode(isDark: Boolean) {
        // Empty
    }
}
package com.touchtalent.bobble.keyboard.sample.services.view

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.touchtalent.bobble.keyboard.sample.databinding.LayoutTopViewBinding

class TopBarView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    private val binding = LayoutTopViewBinding.inflate(LayoutInflater.from(context), this)

    fun updateTimer(time: String) {
        binding.timer.text = time
    }

    fun setColorMode(isDark: Boolean) {
        if (isDark) {
            binding.timer.setTextColor(Color.WHITE)
        } else {
            binding.timer.setTextColor(Color.BLACK)
        }
    }
}